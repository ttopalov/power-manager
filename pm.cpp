/*
 * pm.cpp
 *
 * Created: 30.9.2019 г. 9:01:25
 * Author : Todor Topalov
 */ 

#define F_CPU 1000000
#include <avr/io.h>
#include <util/delay.h>

#define IN_BAT_OK (1 << 2)
#define IN_AC_FAIL (1 << 3)
#define IN_COM_ACTIVITY_IN (1 << 4)
#define INB_REQUEST_COM_ACTIVITY (1 << 2)
#define OUT_SHDN (1 << 0)
#define OUT_PWR (1 << 1)
#define OUT_PWR2 (1 << 7)
#define OUTPUTS (OUT_SHDN | OUT_PWR | OUT_PWR2)

#define LED_BLINK (1 << 0)
#define LED_STATUS (1 << 1)
#define LEDS (LED_STATUS | LED_BLINK)

#define OFF_TIMEOUT 30
#define ON_TIMEOUT 15
#define COM_ACTIVITY_TIMEOUT 30
#define COM_ACTIVITY_RESET_DURATION 5

enum State
{
	down,
	upCountdown,
	up,
	goingDown
};

static void initIO()
{
	PORTA &= ~OUTPUTS;
	DDRA |= OUTPUTS;

	DDRB |= LEDS;
	PORTB &= ~LEDS;

	DDRA &= ~(IN_BAT_OK | IN_AC_FAIL | IN_COM_ACTIVITY_IN);
	PORTA |= (IN_BAT_OK | IN_AC_FAIL);
	
	DDRB &= ~(INB_REQUEST_COM_ACTIVITY);
}

static void setOut(uint8_t out, bool on)
{
	if (on)
		PORTA |= out;
	else
		PORTA &= ~out;
}

static void setLed(uint8_t out, bool on)
{
	if (on)
		PORTB &= ~out;
	else
		PORTB |= out;
}

int main(void)
{
	initIO();
	State state = down;
	short stateTimeout = 0;
	uint8_t ledCntr = 0;
	uint8_t lastComActivityState = 0;
	uint8_t lastReqComActivityState = 0;
	uint16_t lastComActivity = 0;
	bool hasRequestedComActivity = false;

	while (1) 
    {
		// delay 100ms while checking for COM activity
		for (uint16_t i = 0; i < 1000; i++)
		{
			uint8_t currentComActivityState = PINA & IN_COM_ACTIVITY_IN;
			uint8_t currentReqComActivityState = PINB & INB_REQUEST_COM_ACTIVITY;
			if (currentComActivityState != lastComActivityState) {
				lastComActivityState = currentComActivityState;
				lastComActivity = 0;
				hasRequestedComActivity = false;
			}
			if (currentReqComActivityState != lastReqComActivityState) {
				hasRequestedComActivity = true;
				lastReqComActivityState = currentReqComActivityState;
			}
			_delay_us(100);
		}
		setLed(LED_BLINK, ledCntr++ & 8);
		bool batOK = !!(PINA & IN_BAT_OK);
		bool acFail = !!(PINA & IN_AC_FAIL);
		if (stateTimeout)
			stateTimeout--;
		switch (state)
		{
		case down:
			if (!acFail)
			{
				state = upCountdown;
				stateTimeout = ON_TIMEOUT * 10;
			}
			break;
		case upCountdown:
			if (acFail)
				state = down;
			else if (!stateTimeout)
			{
				lastComActivity = 0;
				hasRequestedComActivity = false;
				state = up;
			}
			break;
		case up:
			if (acFail && !batOK)
			{
				state = goingDown;
				stateTimeout = OFF_TIMEOUT * 10;
			} else {
				lastComActivity++;
				if (lastComActivity > 10 * (COM_ACTIVITY_TIMEOUT + COM_ACTIVITY_RESET_DURATION))
				{
					lastComActivity = 0;
					hasRequestedComActivity = false;
				}
			}
			break;
		case goingDown:
			if (!stateTimeout)
				state = down;
			break;
		}

		switch (state)
		{
		case down:
		case upCountdown:
			setOut(OUT_SHDN, false);
			setOut(OUT_PWR, false);
			setOut(OUT_PWR2, false);
			setLed(LED_STATUS, false);
			break;
		case up:
			setOut(OUT_SHDN, false);
			setOut(OUT_PWR, true);
			setOut(OUT_PWR2, !hasRequestedComActivity || (lastComActivity <= 10 * COM_ACTIVITY_TIMEOUT));
			setLed(LED_STATUS, false);
			break;
		case goingDown:
			setOut(OUT_PWR, true);
			setOut(OUT_PWR2, false);
			setOut(OUT_SHDN, stateTimeout >= (OFF_TIMEOUT - 1) * 10);
			setLed(LED_STATUS, true);
			break;
		}
	}
}
